const SUCCESS_HEADER = 'Success!';
const SUCCESS_MESSAGE = 'Изменения были успешно отправлены.\nПотребуется время, чтобы их обработать.';
const ALERT_HEADER = "Alert!";
const NO_PHOTO_SELECTED_MESSAGE = "Вы выбрали 0 фото";

const AWS = window.AWS;

AWS.config.update({
    cors: true
});

const s3 = new AWS.S3({
    accessKeyId: S3_KEY_ID,
    secretAccessKey: S3_ACCESS_KEY,
    region: S3_REGION_NAME,
    endpoint: S3_ENDPOINT
});


async function loadImageFromS3(key) {
    const params = {
        Bucket: S3_BASKET_NAME,
        Key: key
    };
    try {
        const data = await s3.getObject(params).promise();
        let blob;
        const contentType = data.ContentType;
        if (contentType === 'image/jpeg') {
            blob = new Blob([data.Body], {type: 'image/jpeg'});
        } else if (contentType === 'image/jpg') {
            blob = new Blob([data.Body], {type: 'image/jpg'});
        } else {
            throw new Error(`Unsupported image format: ${contentType}`);
        }
        return URL.createObjectURL(blob);

    } catch (err) {
        console.log(err);
        return null;
    }
}


