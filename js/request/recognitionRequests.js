// const requestHeaders = new Headers();
// requestHeaders.append('Authorization', 'Bearer ' + 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoidGVsZWdyYW0iLCJleHAiOjIwNzg3NTI0MjMsImlhdCI6MTcxODc1MjQyM30.pTi6GO0ta83kBnWCXCLtp3LiZP67OzuJ-r5bnmgW0wqlP72kK8oFlkTd-RrfBwyK6f7H_zzuwwublRKT1PdqGiq6h1-tLM--_31kF_-mlyKAmAPHpgL-oYrs_FzLWCMV5XMPVG3zA-N_SQ1W3olCdTua-agsNd39YIIOSsUhRXaLz2Q3MwEAKeMApxN4BJFwVwUUAaYxCtS9ZxMpvS_zhMww8u2vVnf7yx62UaWqChO5d2Nch1pZoJfHj8DXR6BnYSlJ4zYzRrMVPTctHEWRR9Adn4r0tglMgx4oB49lLybm5X0KffZCl2mKiSSyiKVKhOsIOhdVabbaITTOqu2azg');

/**
 * Fetches images associated with a specific person from the API.
 *
 * @param {string} personName - The name of the person.
 * @param {number} clientId - The unique identifier of the client.
 *
 * @returns {Promise<Object>} - A promise that resolves to the fetched data.
 *
 * @throws {Error} - If the fetch operation fails or the response status is not OK.
 *
 * @example
 * getImagesByPersonName('John Doe', 123)
 *   .then(data => console.log(data))
 *   .catch(error => console.error(error));
 */
async function getImagesByPersonName(personName, clientId) {

    const requestParams = new URLSearchParams({
        client_id: clientId,
        person_name: personName,
    });
    const response = await fetch(
        `${API_BASE_URL}${API_RECOGNITION_PREFIX}/image/get/?${requestParams}`,
        {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + API_TOKEN
            }
        }
    );
    if (!response.ok) {
        throw new Error(`HTTP error
    !status
: ${response.status}
    `);
    }
    return await response.json();
}

async function getPersonNames(clientId) {

    const requestParams = new URLSearchParams({
        client_id: clientId
    });
    const response = await fetch(
        `${API_BASE_URL}${API_RECOGNITION_PREFIX}/person/get/all?${requestParams}`,
        {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + API_TOKEN
            }
        }
    );
    if (!response.ok) {
        throw new Error(`HTTP error
    !status
: ${response.status}
    `);
    }
    return await response.json();
}

async function deleteImage(imageId, clientId) {
    const requestParams = new URLSearchParams({
        image_id: imageId,
        client_id: clientId
    });
    const response = await fetch(
        `${API_BASE_URL}${API_RECOGNITION_PREFIX}/image/delete/?${requestParams}`,
        {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + API_TOKEN
            }
        }
    );
    if (!response.ok) {
        throw new Error(`HTTP error
    !status
: ${response.status}
    `);
    }
    return  response;
}

async function setImage(clientId, personName, key, imageId = null) {
    const requestParams = new URLSearchParams({
        key: key,
        client_id: clientId,
        person_name: personName,
        image_id: imageId
    });
    const response = await fetch(
        `${API_BASE_URL}${API_RECOGNITION_PREFIX}/image/set?${requestParams}`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + API_TOKEN
            }
        }
    );
    if (!response.ok) {
        throw new Error(`HTTP error!status: ${response.status} `);
    }
    return  response;
}