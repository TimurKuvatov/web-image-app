const selectAllButton = document.getElementById('select-all-button');

function changeSelectionMode(isActive) {
    const galleryMothDates = document.querySelectorAll('.gallery-month-date');
    const galleryMothSelectAllIcons = document.querySelectorAll('.gallery-month-select-all-icon');

    galleryMothDates.forEach(date => {
        if (isActive) {
            date.classList.add('select-mode');
        } else {
            date.classList.remove('select-mode');
        }
    })
    galleryMothSelectAllIcons.forEach(icon => {
        icon.style.display = isActive? "flex" : "none";
    })
}

function changeMonthImagesSelection(gallerySelectAllMonthImages) {
    const img = gallerySelectAllMonthImages.getElementsByTagName('img')[0];
    if (img.style.visibility === "hidden") {
        img.style.visibility = "visible";
        selectionMode = true;
        (gallerySelectAllMonthImages.parentElement.parentElement.querySelectorAll('img.image:not(.smaller)')).forEach(image => {
            addIcon(image);
            changeImageSelection(image, image.parentElement.parentElement.querySelector('.icon'));
        })
    } else {
        img.style.visibility = "hidden";
        (gallerySelectAllMonthImages.parentElement.parentElement.querySelectorAll('img.image.smaller')).forEach(image => {
            changeImageSelection(image, image.parentElement.parentElement.querySelector('.icon'));
        });
    }
}


// selectAllButton.addEventListener('click', function (event) {
//     let isActive = event.target.getAttribute('src') === '../../images/active-icon.png';
//     event.target.setAttribute('src', isActive ? '../../images/inactive-icon.png' : '../../images/active-icon.png');
//     event.target.classList.toggle('hover');
//     selectionMode = !isActive;
//     if (isActive) {
//         cancelImageSelection();
//     } else {
//         selectAllImages();
//     }
// })