const downloadButton = document.getElementById('download-button');

downloadButton.addEventListener('click', async function (event) {

    const selectedImages = getSelectedImages();
    let urls = [];
    selectedImages.forEach(function (image) {
        urls.push(image.src);
    })
    const promises = urls.map(async url => {
        return (await fetch(url)).blob();
    });
    const blobs = await Promise.all(promises);
    const zip = new JSZip();
    let img = zip.folder("images");
    for (const blob of blobs) {
        const index = blobs.indexOf(blob);
        img.file(`${personName}_${index}.jpg`, new File([blob], 'filename.jpg'), {base64: true});
    }
    const zipFile = await zip.generateAsync({type: 'blob'});
    const a =document.createElement('a');
    a.download = `${personName}.zip`;
    const url = URL.createObjectURL(zipFile);
    a.href = url;
    a.style.display = 'none';
    document.body.appendChild(a);
    a.click();
    a.remove();
    URL.revokeObjectURL(url);
});
