$(window).on('scroll', function() {
  var lastGalleryMonth = $('.gallery-month:last');
  var scrollPosition = $(window).scrollTop() + $(window).height();
  var lastGalleryMonthBottom = lastGalleryMonth.offset().top + lastGalleryMonth.height();

  if (scrollPosition > lastGalleryMonthBottom) {
    $(window).scrollTop(lastGalleryMonthBottom - $(window).height());
  }
});