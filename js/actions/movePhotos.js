const moveButton = document.getElementById('move-button');
moveButton.addEventListener('click', async function (event) {
    event.stopPropagation();
    event.preventDefault();
    if (submenu.style.visibility === 'visible') {
        submenu.style.visibility = 'hidden';
        return;
    }
    closeAllSubmenu();
    await createPersonNamesSubmenu(submenu, "move");
});