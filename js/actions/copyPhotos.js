const copyButton = document.getElementById('copy-button');
const submenu = document.querySelector ('.submenu');


copyButton.addEventListener('click', async function (event) {
    event.stopPropagation();
    event.preventDefault();
    if (submenu.style.visibility === 'visible') {
        submenu.style.visibility = 'hidden';
        return;
    }
    closeAllSubmenu();
    await createPersonNamesSubmenu(submenu,"copy");
});