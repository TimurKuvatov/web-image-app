const deleteButton = document.getElementById('delete-button');
deleteButton.addEventListener('click', async function (event) {
    let userResponse = confirm('Вы действительно хотите безвозвратно удалить эти фото?');
    if (userResponse) {
        const selectedImages = getSelectedImages();
        let imageContainer = document.getElementById('image-container');
        for (const image of selectedImages) {
            await deleteImage(image.dataset.id, clientId);
            image.parentElement.parentElement.remove();
        }
        calculateSelectedImages();
    }
});