let _selectionMode = false;
const cancelSelectionButton = document.getElementById('cancel-selection-button');

Object.defineProperty(this, 'selectionMode', {
    get: function () {
        return _selectionMode;
    },
    set: function (value) {
        changeSelectionMode(value);
        _selectionMode = value;
        if (value === true) {
            cancelSelectionButton.style.visibility = 'visible';
        } else {
            cancelSelectionButton.style.visibility = 'hidden';
            document.querySelectorAll('.gallery-month-select-all-icon img').forEach(image => {
                image.style.visibility = 'hidden';
            })
            // selectAllButton.setAttribute('src', '../../images/inactive-icon.png');
        }
    }
});

function cancelImageSelection(event) {
    (document.querySelectorAll('.image.smaller')).forEach(image => {
        changeImageSelection(image, image.parentElement.parentElement.querySelector('.icon'));
    })
    selectionMode = false;
}

function getSelectedImages() {
    return document.querySelectorAll('.image.smaller');
}

let startTime;

function changeImageSelection(image, icon) {
    image.classList.toggle('smaller');
    icon.getElementsByTagName('img')[0].setAttribute('src', icon.getAttribute('src') === '../../images/active-icon.png' ? '../../images/inactive-icon.png' : '../../images/active-icon.png');
    if (image.hasAttribute('smaller')) {
        icon.classList.add('hover');
        icon.style.opacity = 1;
    } else {
        icon.classList.toggle('hover');
        icon.style.opacity = 0.5;
    }
    let selectedImages = document.querySelectorAll('.image.smaller');
    selectionMode = selectedImages.length !== 0;
    if (!image.classList.contains('smaller')) {
        let icon = image.parentElement.parentElement.querySelector('.icon');
        if (icon && !icon.contains(event.relatedTarget) && !image.classList.contains('smaller')) {
            image.parentElement.parentElement.removeChild(icon);
        }
    }
    calculateSelectedImages();
}

function addIcon(image) {
    let icon = fromStringHTML(iconTemplateHtml(false), true);
    if (!image.parentElement.parentElement.querySelector(`.icon`)) {
        image.parentElement.parentElement.appendChild(icon);
        icon.addEventListener('click', function (event) {
            event.stopPropagation();
            event.preventDefault();
            let image = icon.parentElement.querySelector('.image')
            changeImageSelection(image, icon);
        });
    }
}

let pressTimer;


function handleMouseDown(event) {
    pressTimer = window.setTimeout(function () {
        event.stopPropagation();
        event.preventDefault();
        if(event.target.classList.contains('smaller')){
            return;
        }
        if (!event.target.parentElement.parentElement.querySelector('.icon')) {
            addIcon(event.target);
        }
        changeImageSelection(event.target, event.target.parentElement.parentElement.querySelector('.icon'));
    }, 700);
    return false;
}

function handleMouseUp(event) {
    clearTimeout(pressTimer);
    return false;
}

function addImageEvents() {
    const images = document.querySelectorAll('.image');
    images.forEach(image => {
        image.addEventListener('mouseover', function () {
            if (!selectionMode) {
                addIcon(image);
            }
        });
        image.addEventListener('mouseout', function (event) {
            if (!selectionMode && image.parentElement.parentElement.querySelector('.icon')) {
                let icon = image.parentElement.parentElement.querySelector('.icon');
                if (!icon.contains(event.relatedTarget) && !image.classList.contains('smaller')) {
                    image.parentElement.parentElement.removeChild(icon);
                }
            }
        });

        image.addEventListener('click', function (event) {
            startTime = new Date().getTime();
            if (selectionMode) {
                event.stopPropagation();
                event.preventDefault();
                if (!image.parentElement.parentElement.querySelector('.icon')) {
                    addIcon(image);
                }
                changeImageSelection(image, image.parentElement.parentElement.querySelector('.icon'));
            }
        });

        image.addEventListener('mousedown', handleMouseDown);
        image.addEventListener('touchstart', handleMouseDown);

        image.addEventListener('mouseup', handleMouseUp);
        image.addEventListener('touchend', handleMouseUp);
    })
}


cancelSelectionButton.addEventListener('click', function (event) {
    cancelImageSelection();
});
