async function createPersonNamesSubmenu(element, action) {
    let names = await getPersonNames(clientId);
    const flkty = new Flickity('.submenu');
    flkty.remove(document.querySelectorAll('.carousel-cell'));
    flkty.reloadCells();
    element.style.visibility = 'visible';
    names.forEach(function (name) {
        const div = document.createElement('div');
        div.textContent = name;
        div.classList.add('carousel-cell');
        div.classList.add('is-selected');
        div.dataset.action = action;
        div.addEventListener('click', async function (event) {
            if (event.target.closest('.flickity-button-icon')) {
                event.stopPropagation();
            }
            let action = div.dataset.action;
            const selectedImages = getSelectedImages();
            closeAllSubmenu();
            if (selectedImages.length === 0) {
                renderMessage(NO_PHOTO_SELECTED_MESSAGE, ALERT_HEADER);
                return;
            }
            renderMessage(SUCCESS_MESSAGE, SUCCESS_HEADER);
            cancelImageSelection();
            for (const image of selectedImages) {
                if (action === 'copy') {
                    await setImage(clientId, event.target.textContent, image.dataset.key, image.dataset.id);
                } else if (action === 'move') {
                    await setImage(clientId, event.target.textContent, image.dataset.key, image.dataset.id);
                    await deleteImage(image.dataset.id, clientId);
                    image.parentElement.parentElement.remove();
                }
            }
        })
        flkty.append(div);
    })
}