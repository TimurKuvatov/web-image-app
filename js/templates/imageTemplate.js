
async function createImageHTML(key, imageId) {
    const imageSrc = (await loadImageFromS3(key)).toString();
     return '<div class="image-wrapper">' +
        '<a href="' + imageSrc + '" data-lightbox="models">' +
            '<img src="' + imageSrc + '" alt="" class="gallery-month-image image" data-id="' + imageId + '" data-key="' + key + '"/>' +
        '</a>' +
        '</div>';
}

