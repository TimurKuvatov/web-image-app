function iconTemplateHtml(isActive) {
    return `<div class="icon">
  <img src="${isActive? '../../images/active-icon.png' : '../../images/inactive-icon.png'}" alt="Image">
</div>`;
}

