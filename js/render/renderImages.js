imageContainer = document.getElementById('image-container');

async function renderImages(personName, clientId) {
    const images = await getImagesByPersonName(personName, clientId);
    const groupedImages = images.reduce((acc, current) => {
        let datetime = moment(current.date).locale('ru');
        const monthName = datetime.format('MMMM');
        const yearNum = datetime.year();
        const date = `${monthName} ${yearNum}г.`;
        if (!acc[date]) {
            acc[date] = [];
        }
        acc[date].push(current);
        return acc;
    }, {});
    for (const [date, images] of Object.entries(groupedImages)) {
        let galleryMonth = document.createElement('div');
        galleryMonth.classList.add('gallery-month');
        let galleryMonthTitle = document.createElement('div');
        galleryMonthTitle.classList.add('gallery-month-title');
        let galleryMonthDate = document.createElement('div');
        galleryMonthDate.classList.add('gallery-month-date');
        galleryMonthDate.textContent = date;
        galleryMonth.appendChild(galleryMonthTitle);
        let galleryMonthImageContainer = document.createElement('div');
        galleryMonthImageContainer.classList.add('gallery-month-image-container');
        galleryMonth.appendChild(galleryMonthImageContainer);
        let gallerySelectAllMonthImages = document.createElement('div');
        gallerySelectAllMonthImages.classList.add('gallery-month-select-all-icon');
        const activeIconImg = document.createElement('img');
        activeIconImg.src = "../../images/active-icon.png";
        gallerySelectAllMonthImages.appendChild(activeIconImg);
        galleryMonthTitle.appendChild(gallerySelectAllMonthImages);
        galleryMonthTitle.appendChild(galleryMonthDate);
        gallerySelectAllMonthImages.addEventListener('click', event => {
            changeMonthImagesSelection(gallerySelectAllMonthImages);
        })
        for (const image of images) {
            const key = image.key;
            galleryMonthImageContainer.insertAdjacentHTML('beforeend', await createImageHTML(key, image.id));
        }
        imageContainer.appendChild(galleryMonth);
    }

    addImageEvents();
}



