const button = document.querySelector("button");
const toast = document.querySelector(".toast");
const closeIcon = document.querySelector(".close");
const progress = document.querySelector(".progress");
const message = document.getElementById("message-text");
const messageHeader = document.getElementById("message-header");
const messageIcon = document.getElementById("message-icon");

let timerStart, timerEnd;

function renderMessage(messageText, messageHeaderText) {
    message.innerHTML = messageText;
    messageHeader.innerHTML = messageHeaderText;
    if (messageIcon.classList.contains("fa-times")) {
        messageIcon.classList.remove("fa-times");
    }
    if (messageIcon.classList.contains("fa-check")) {
        messageIcon.classList.remove("fa-check");
    }
    const root = document.querySelector(':root');

    if (messageHeaderText === SUCCESS_HEADER) {
        messageIcon.classList.add("fa-check");
        messageIcon.style.backgroundColor = getComputedStyle(root).getPropertyValue('--success-bg-color');
    } else if (messageHeaderText === ALERT_HEADER) {
        messageIcon.classList.add("fa-times");
        messageIcon.style.backgroundColor = getComputedStyle(root).getPropertyValue('--alert-bg-color');
    }
    toast.classList.add("active");
    toast.style.visibility = 'visible';
    progress.classList.add("active");

    timerStart = setTimeout(() => {
        toast.classList.remove("active");
    }, 5000);

    timerEnd = setTimeout(() => {
        progress.classList.remove("active");
        toast.style.visibility = 'hidden';
    }, 5300);
}

closeIcon.addEventListener("click", () => {
    toast.classList.remove("active");

    setTimeout(() => {
        progress.classList.remove("active");
        toast.style.visibility = 'hidden';
    }, 300);

    clearTimeout(timerStart);
    clearTimeout(timerEnd);
});